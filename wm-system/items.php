<?php
include('user_session.php');
include('warehouse_functions.php');

$isAdmin = checkSession($_SESSION['isAdmin']);

require "conn.php";
$query = "SELECT *  FROM supplies";
$result = mysqli_query($conn, $query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.3/css/dataTables.bootstrap5.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <link rel="stylesheet" href="style/items.css" />
  <link href="lineicons/web-font-files/lineicons.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/bootstrap.bundle.min.js">
  <title>Item Management</title>
  
</head>
<body>
<header class="header-section text-center">
        <h1>Warehouse Management System with 2D Layout</h1>
    </header>
    <div class="wrapper">
         <!-- SIDE NAVBAR -->
         <?php include "side_navbar.php" ?>
        <!-- SIDE NAVBAR -->
       
    <div class="main--content">
        <div class="header--wrapper">
            <div class="header--title">
                <span>Inventory Management</span>
                <h2>Supply Items</h2>
                
            </div>
      
        </div>
        <div class="card-container">
            <h3 class="main-title">New Supply Items</h3>
            <?php include('message.php')?>
            
            <form action="itemsfunc.php" id="form" method="post">

            <label for="itemtype">Item / Type:</label>
  <input type="text" id="itemtype" name="itemtype">
  <label for="class">Classification:</label>
  <input type="text" id="class" name="class">
  
  <label for="unit">Unit:</label>
  <input type="text" id="unit" name="unit" >
  <label for="quan">Quantity:</label>
  <input type="text" id="quan" name="quan" step="0.01">
<input type='hidden' id='active' name='active' value='<?= $isAdmin == 1 ? true : false; ?>'>
  
  
  <input type="submit" value="Add" name="save">
            </form>
            
        </div>
        <div class="tabular-wrapper">
            <h3 class="main-title">
                Items
            </h3>
            <div class="table-container">
            <table id="example" class="table table-striped" style="width:100%">
            <thead>
                <tr>
                   <?= $isAdmin == 1 ?  '<th>Action</th>' : ''  ?> 
                    <th>Item / Type</th>
                    <th>Classification</th>
                    <th>Unit</th>
                    <th>Quantity</th>
                    <th>Timestamp</th>
                  
                </tr>
            </thead>
            <tbody>
            <?php
                            // $query = "SELECT * FROM  supplies";
                            // $query_run = mysqli_query($conn, $query);

                            // if (mysqli_num_rows($query_run)>0) {
                                $items = getAllItems($conn, $isAdmin);
                                if($items){
                                foreach($items as $items){

                                    ?>
                                    <tr>
                                    <?php if($isAdmin == 1) { ?><td>
    
                                    <form action="itemsfunc.php" method="POST" class="d-inline" onsubmit="return confirm('Are you sure you want to delete this item?');">
                                      <button type="submit" name="delete_group" value="<?=$items['id']?>;" style="color:red; background:none;">Delete</button>
                                        </form>
                                        
                                        <a href="itemsedit.php?id=<?= $items['item'];?>" type="submit" name="Update" style="color:blue; background:none;" >Edit</a>
                                       
                                       
                                      
                                    </td> <?php } ?>
                                       
                                        <td><?= $items['item']?></td>
                                        <td><?= $items['classification']?></td>
                                        <td><?= $items['unit']?></td>
                                        <td><?= $items['quantity']?></td>
                                        <td><?= $items['created_at']?></td>
                                        
                                      
                                     
                                    </tr>
                                    <?php
                                }
                            }else{
                                echo "<h5> No record Found</h5>";
                            }
                            ?>
            </tbody>
            <tfoot>
                <tr>
                <?= $isAdmin == 1 ?  '<th>Action</th>' : ''  ?>   
                    <th>Item / Type</th>
                    <th>Classification</th>
                    <th>Unit</th>
                    <th>Quantity</th>
                    <th>Timestamp</th>
                </tr>
            </tfoot>
        </table>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.bootstrap5.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
    <script src="functions/inventory.js"></script>
</body>
</html>