<?php
session_start();

function useSessionAdmin($value = null, $id)
{
$_SESSION['isAdmin'] = $value;
$_SESSION['userId'] = $id;

$userSession = array($_SESSION['isAdmin']);

return $userSession;
}

function checkSession($value){
  if(!isset($_SESSION['isAdmin'])){
    return header('location:login.php');
}

return $value;

}

?>