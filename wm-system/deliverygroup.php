<?php
include('user_session.php');
include('warehouse_functions.php');

$isAdmin = checkSession($_SESSION['isAdmin']);
include "conn.php";
$query = "SELECT *  FROM items";
$result = mysqli_query($conn, $query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.3/css/dataTables.bootstrap5.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <link rel="stylesheet" href="style/items.css" />
  <link href="lineicons/web-font-files/lineicons.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/bootstrap.bundle.min.js">
  <title>Item Management</title>
  
</head>
<body>
<header class="header-section text-center">
        <h1>Warehouse Management System with 2D Layout</h1>
    </header>
    <div class="wrapper">
         <!-- SIDE NAVBAR -->
         <?php include "side_navbar.php" ?>
        <!-- SIDE NAVBAR -->
       
    <div class="main--content">
        <div class="header--wrapper">
            <div class="header--title">
                <span>Inventory Management</span>
                <h2>Delivery Groups</h2>
                
            </div>
       
        </div>
        <div class="card-container">
            <h3 class="main-title">New Delivery Groups</h3>
            <?php include('message.php')?>
            <form action="deliverygroupfunc.php" id=form method="post">

            <label for="name">Name:</label>
  <input type="text" id="name" name="name" required>
  <label for="deliverydate">Date of Delivery:</label>
  <input type="date" id="deliverydate" name="deliverydate" required>
  <label for="deliveredby">Delivered By:</label>
  <input type="text" id="deliveredby" name="deliveredby" required>
  <label for="recievedby">Recieved By:</label>
  <input type="text" id="recievedby" name="recievedby" required>
  <input type='hidden' id='status' name='status' value='<?= $isAdmin == 1 ? true : false; ?>'>
  
  <input type="submit" name="save" value="Add">
            </form>
            
        </div>
        <div class="tabular-wrapper">
            <h3 class="main-title">
                Delivery Groups
            </h3>
            <div class="table-container">
            <table id="example" class="table table-striped" style="width:100%">
            <thead>
                <tr>
               <?= $isAdmin == 1 ? '<th>Action</th>' : '' ?>   
                    <th>Name</th>
                    <th>Date of Delivery</th>
                    <th>Delivered By</th>
                    <th>Recieved By</th>
                    <th>Status</th>
                    <th>Timestamp</th>
                </tr>
            </thead>
            <tbody>
            <?php
                            // $query = "SELECT * FROM  delivery_groups";
                            // $query_run = mysqli_query($conn, $query);
                            $items = getAllDelivery($conn, $isAdmin);
                            if ($items) {
                                foreach($items as $items){

                                    ?>
                                    <tr>
                                  <?php if($isAdmin == 1) { ?> <td>
                                        <form action="deliverygroupfunc.php" method="POST" class="d-inline" onsubmit="return confirm('Are you sure you want to delete this item?');">
                                      <button type="submit" name="delete_group" value="<?=$items['id']?>;" style="color:red; background:none; border:none;">Delete</button>
                                        </form>
                                        <a href="supplygroupupdate.php?id=<?= $items['id'];?>" type="submit" name="Update" style="color:blue; background:none;" >Edit</a>
                                        <a href="supplygroupupdate.php?id=<?= $items['name'];?>" type="submit" name="Update" style="color:rgb(15, 99, 15); background:none;" >Deliveries</a>
                                       
                                      
                                    </td> <?php } ?>
                                        <td><?= $items['name']?></td>
                                        <td><?= $items['date_of_delivery']?></td>
                                        <td><?= $items['delivered_by']?></td>
                                        <td><?= $items['recieved_by']?></td>
                                        <td><?= $items['active'] == 1 ? 'Active' : 'Inactive' ?></td>
                                        <td><?= $items['created_at']?></td>

                                    </tr>
                                    <?php
                                }
                            }else{
                                echo "<h5> No record Found</h5>";
                            }
                            ?>
            </tbody>
            <tfoot>
                <tr>
                <?= $isAdmin == 1 ? '<th>Action</th>' : '' ?>      
                    <th>Name</th>
                    <th>Date of Delivery</th>
                    <th>Delivered By</th>
                    <th>Recieved By</th>
                    <th>Status</th>
                    <th>Timestamp</th>
                </tr>
            </tfoot>
        </table>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.bootstrap5.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
    <script src="functions/inventory.js"></script>
</body>
</html>