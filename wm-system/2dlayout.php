<?php
include('user_session.php');
include('warehouse_functions.php');

$isAdmin = checkSession($_SESSION['isAdmin']);

require "conn.php";
$query = "SELECT *  FROM supplies";
$result = mysqli_query($conn, $query);

if(isset($_POST['edit-location'])) {

  if(isset($_POST['products']) && isset($_POST['location'])) {
    $setLocation = setItemSection($conn, $_POST['products'], $_POST['location']);

    if($setLocation) {
      $_SESSION['message'] = null;
      header("Location: 2dlayout.php");
      exit();
    } else {
      $_SESSION['message'] = "Failed to update product location";
      header("Location: 2dlayout.php");
      exit();
    }
  }
}

if(isset($_POST['remove'])){

  if(isset($_POST['id'])) {
    $unsetLocation = setItemSection($conn, $_POST['id'], 0);

    if($unsetLocation) {
      $_SESSION['message'] = null;
      header("Location: 2dlayout.php");
      exit();}
      else {
        $_SESSION['message'] = "Failed to update product location";
        header("Location: 2dlayout.php");
        exit();
      }
  }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" href="style/select2.min.css">
  <link rel="stylesheet" href="style/2dlayout.css" />
  <link href="lineicons/web-font-files/lineicons.css" rel="stylesheet" />
  <title>Inventory Management</title>
</head>
<body>
<header class="header-section text-center">
        <h1>Warehouse Management System with 2D Layout</h1>
    </header>
    <div class="wrapper">
        <!-- SIDE NAVBAR -->
        <?php include "side_navbar.php" ?>
        <!-- SIDE NAVBAR -->


        <div class="main--content">
        <div class="header--wrapper">
            <div class="header--title">
                <span>Inventory Management</span>
                <h2>2D Layout</h2>
            </div>
        </div>
        <!-- CONTENT HERE -->

        <div class="select-container">


        <?php if($isAdmin == 1 ) { ?>
        <div id="select-container1" class='mx-auto'>
          <?= isset($_SESSION['message']) ? $_SESSION['message'] : ''; ?>

        <?php
                $selection = getItemsSection($conn, 0);

        ?>

        <form action="#" method='POST'>
        <label for="products">Product Placement: </label>
        <select name="products" id="products">
            <option selected disabled>Select Product</option> <!-- Placeholder option -->
           <?php 
           foreach($selection as $item) {
                ?>
                    <option value="<?= $item['id'] ?>"><?= $item['Name'] ?></option>
                <?php
                }
                  ?>
        </select>
        <select name="location" id="location">
            <option selected disabled>Select Location</option>
            <option value="1">Section 1</option>
            <option value="2">Section 2</option>
            <option value="3">Section 3</option>
            <option value="4">Section 4</option>
            <option value="5">Section 5</option>
            <option value="6">Section 6</option>
            <option value="7">Section 7</option> <!-- Placeholder option -->
        </select>
      <button id="edit-location" name="edit-location">Edit Product Location</button>
       </form>
        </div>
        <?php } ?>
              <div class='container-fluid mx-auto'>

              <!-- ROW 1 -->
                <div class="row m-4 ps-md-5">
                    <div class="col-12 col-md-4">
                        <div class="card text-center" style="width: 18rem; height: 16rem">
                          <div class="card-body d-flex flex-column justify-content-center align-items-center text-center">
                            <h6 class="card-title btn text-primary" <?= $isAdmin == 1 ? 'data-bs-toggle="modal"' : '' ?>  data-bs-target="#modal-1"><strong>SECTION 1</strong></h6>
                            <?php
                            $section1 = getItemsSection($conn, 1);
                            ?>
                            <ul class='list-unstyled text-center small'>
                            <?php
                            if($section1) {
                            foreach($section1 as $item) {
                                ?>
                                    <li class="text-uppercase text-danger"><?= $item['Name'] ?></li>
                                <?php
                                }
                              }
                              else {
                                ?>
                                <p class='text-center' align="center">No items in this section</p>

                                <?php
                              }
                                ?>
                            </ul>
                          </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-4">
                        <div class="card" align="center" style="width: 18rem; height: 16rem">
                          <div class="card-body d-flex flex-column justify-content-center align-items-center">
                          <h6 class="card-title btn text-primary" <?= $isAdmin == 1 ? 'data-bs-toggle="modal"' : '' ?>  data-bs-target="#modal-2"><strong>SECTION 2</strong></h6>
                            <?php
                            $section2 = getItemsSection($conn, 2);
                            ?>
                            <ul class='list-unstyled text-center small'>
                            <?php
                            if($section2) {
                            foreach($section2 as $item) {
                                ?>
                                    <li class="text-uppercase text-danger"><?= $item['Name'] ?></li>
                                <?php
                                }
                              }
                              else {
                                ?>
                                <p align="center">No items in this section</p>

                                <?php
                              }
                                ?>
                            </ul>
                          </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-4">
                        <div class="card" align="center" style="width: 18rem; height: 16rem">
                          <div class="card-body d-flex flex-column justify-content-center align-items-center">
                          <h6 class="card-title btn text-primary" <?= $isAdmin == 1 ? 'data-bs-toggle="modal"' : '' ?>  data-bs-target="#modal-3"><strong>SECTION 3</strong></h6>
                            <?php
                            $section3 = getItemsSection($conn, 3);
                            ?>
                            <ul class='list-unstyled text-center small'>
                            <?php
                            if($section3) {
                            foreach($section3 as $item) {
                                ?>
                                    <li class="text-uppercase text-danger"><?= $item['Name'] ?></li>
                                <?php
                                }
                              }
                              else {
                                ?>
                                <p align="center">No items in this section</p>

                                <?php
                              }
                                ?>
                            </ul>
                          </div>
                        </div>
                    </div>

                </div>
                <!-- ROW 1 -->
                <!-- ROW 2 -->
                <div class="row m-4 ps-md-5">
                    <div class="col-12 col-md-4">
                        <div class="card" align="center" style="width: 18rem; height: 16rem">
                          <div class="card-body d-flex flex-column justify-content-center align-items-center">
                          <h6 class="card-title btn text-primary" <?= $isAdmin == 1 ? 'data-bs-toggle="modal"' : '' ?>  data-bs-target="#modal-4"><strong>SECTION 4</strong></h6>
                            <?php
                            $section4 = getItemsSection($conn, 4);
                            ?>
                            <ul class='list-unstyled text-center small'>
                            <?php
                            if($section4) {
                            foreach($section4 as $item) {
                                ?>
                                    <li class="text-uppercase text-danger"><?= $item['Name'] ?></li>
                                <?php
                                }
                              }
                              else {
                                ?>
                                <p align="center">No items in this section</p>

                                <?php
                              }
                                ?>
                            </ul>
                          </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-4">
                        <div class="card" align="center" style="width: 18rem; height: 16rem">
                          <div class="card-body d-flex flex-column justify-content-center align-items-center">
                          <h6 class="card-title btn text-primary" <?= $isAdmin == 1 ? 'data-bs-toggle="modal"' : '' ?>  data-bs-target="#modal-5"><strong>SECTION 5</strong></h6>
                            <?php
                            $section5 = getItemsSection($conn, 5);
                            ?>
                            <ul class='list-unstyled text-center small'>
                            <?php
                            if($section5) {
                            foreach($section5 as $item) {
                                ?>
                                    <li class="text-uppercase text-danger"><?= $item['Name'] ?></li>
                                <?php
                                }
                              }
                              else {
                                ?>
                                <p align="center">No items in this section</p>

                                <?php
                              }
                                ?>
                            </ul>
                          </div>
                        </div>
                    </div>

                    <div class="col-12 col-md-4">
                        <div class="card" align="center" style="width: 18rem; height: 16rem">
                          <div class="card-body d-flex flex-column justify-content-center align-items-center">
                          <h6 class="card-title btn text-primary" <?= $isAdmin == 1 ? 'data-bs-toggle="modal"' : '' ?> data-bs-target="#modal-6"><strong>SECTION 6</strong></h6>
                            <?php
                            $section6 = getItemsSection($conn, 6);
                            ?>
                            <ul class='list-unstyled text-center small'>
                            <?php
                            if($section6) {
                            foreach($section6 as $item) {
                                ?>
                                    <li class="text-uppercase text-danger"><?= $item['Name'] ?></li>
                                <?php
                                }
                              }
                              else {
                                ?>
                                <p align="center">No items in this section</p>

                                <?php
                              }
                                ?>
                            </ul>
                          </div>
                        </div>
                    </div>

                </div>
                <!-- ROW 2 -->
                <!-- ROW 3 -->
                <div class="row m-4">
                    <div class="col-12">
                        <div class="card" align="center" style="width: 84rem; height: 12rem">
                          <div class="card-body d-flex flex-column justify-content-center align-items-center">
                          <h6 class="card-title btn text-primary" <?= $isAdmin == 1 ? 'data-bs-toggle="modal"' : '' ?>  data-bs-target="#modal-7"><strong>SECTION 7</strong></h6>
                            <?php
                            $section7 = getItemsSection($conn, 7);
                            ?>
                            <ul class='list-unstyled text-center small'>
                            <?php
                            if($section7) {
                            foreach($section7 as $item) {
                                ?>
                                    <li class="text-uppercase text-danger"><?= $item['Name'] ?></li>
                                <?php
                                }
                              }
                              else {
                                ?>
                                <p align="center">No items in this section</p>

                                <?php
                              }
                                ?>
                            </ul>
                          </div>
                        </div>
                    </div>
                </div>
                <!-- ROW 3 -->

              </div>

</div>
        <!-- END CONTENT -->
  </div>
</div>


<!-- MODAL -->

<?php
$modalSection = getAllSection($conn);

if($modalSection) {
  foreach($modalSection as $section) {
?>
<div class="modal fade" id="modal-<?= $section['section']?>" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title text-primary pe-none" id="exampleModalLabel"><strong>SECTION <?= $section['section']?></strong></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <div class="container">
        <ul class='list-unstyled text-center small'>
          <?php
          $items = getItemsSection($conn, $section['section']);
          if($items) {
            foreach($items as $item) {
              ?>
              <li class='d-flex gap-1 align-items-center justify-items-center'  data-bs-toggle="tooltip" data-bs-placement="top" title="Remove on Section">
              <form action="#" method="POST">
                <input type="hidden" name="id" value="<?= $item['id'] ?>">
                <button type='submit' id='remove' name='remove' class="btn text-danger text-uppercase"><?= $item['Name'] ?></button>
              </form>
            </li>
              <?php
            }
          }
          ?>
        </ul>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php 
  }
} 

?>
<!-- END MODAL -->

<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js" integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF" crossorigin="anonymous"></script>
</body>
</html>