-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 03, 2024 at 01:26 AM
-- Server version: 10.4.32-MariaDB
-- PHP Version: 8.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `warehouse`
--

-- --------------------------------------------------------

--
-- Table structure for table `delivery_groups`
--

CREATE TABLE `delivery_groups` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_delivery` date NOT NULL,
  `month` varchar(12) NOT NULL,
  `year` varchar(4) NOT NULL,
  `delivered_by` varchar(255) NOT NULL,
  `recieved_by` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `delivery_groups`
--

INSERT INTO `delivery_groups` (`id`, `name`, `date_of_delivery`, `month`, `year`, `delivered_by`, `recieved_by`, `active`, `created_at`, `updated_at`) VALUES
(7, 'clea', '2024-04-13', 'April', '2022', 'darwin', '', 0, '2024-04-12 23:49:16', '2024-04-12 23:49:16'),
(8, 'clea', '0000-00-00', 'April', '2023', 'darwin', '', 0, '2024-04-13 00:14:54', '2024-04-13 00:14:54'),
(9, 'Uniqlo', '0000-00-00', 'May', '2024', 'darwin', 'clea', 0, '2024-04-13 00:24:44', '2024-04-13 00:24:44'),
(10, '003', '2024-05-27', 'May', '2024', 'asdasd', 'asdasda', 1, '2024-05-27 02:39:42', '2024-05-27 02:39:42');

-- --------------------------------------------------------

--
-- Table structure for table `equipment_groups`
--

CREATE TABLE `equipment_groups` (
  `id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `month` varchar(12) NOT NULL,
  `year` varchar(4) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `equipment_groups`
--

INSERT INTO `equipment_groups` (`id`, `name`, `description`, `month`, `year`, `active`, `Timestamp`) VALUES
(1, 'adda', 'dadadadada', 'April', '2024', 0, '2024-05-15 11:36:06'),
(2, 'asdsa', 'asdasd', 'May', '2024', 1, '2024-05-27 03:07:40');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(11) NOT NULL,
  `Name` varchar(50) NOT NULL,
  `Description` varchar(50) NOT NULL,
  `month` varchar(12) NOT NULL,
  `year` varchar(4) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `section` int(11) NOT NULL,
  `Timestamp` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `Name`, `Description`, `month`, `year`, `active`, `section`, `Timestamp`) VALUES
(14, 'stuff toy', 'Bear', '', '', 1, 7, '2024-05-01 03:36:29'),
(15, 'Springs', 'Water Bottles', 'May', '2024', 1, 4, '2024-05-27 01:35:43'),
(16, 'Springss', 'Water Bottless', 'May', '2024', 1, 2, '2024-05-27 01:36:50'),
(17, 'Water Bottless', 'Springss', 'May', '2024', 1, 3, '2024-05-27 01:37:48'),
(23, 'Spring', 'Bottles', 'May', '2024', 1, 1, '2024-05-27 12:45:36');

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`username`, `password`) VALUES
('user', '456'),
('admin', '123');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `username`, `password`) VALUES
(1, 'user456', '456'),
(2, 'admin123', '123'),
(3, 'dada1234', 'dada12345'),
(4, 'dadafafa', 'dardar'),
(5, 'clea123', 'baby'),
(6, 'bebeluvs', 'dadada'),
(7, 'admin12345', 'password');

-- --------------------------------------------------------

--
-- Table structure for table `registered`
--

CREATE TABLE `registered` (
  `id` int(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `contactnumber` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `registered`
--

INSERT INTO `registered` (`id`, `name`, `username`, `contactnumber`) VALUES
(1, 'Darwin', 'Darwin123', '09090909090'),
(2, 'Darwin123456', 'dadafafa', '09090909095'),
(4, 'cleababelove', 'bebeluvs', '09090909090'),
(5, 'Darwincleadardarjoyjoy', 'admin12345', '09090909090');

-- --------------------------------------------------------

--
-- Table structure for table `supplies`
--

CREATE TABLE `supplies` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `item` varchar(255) NOT NULL,
  `classification` varchar(255) DEFAULT NULL,
  `unit` varchar(255) DEFAULT NULL,
  `quantity` varchar(255) NOT NULL,
  `month` varchar(12) NOT NULL,
  `year` varchar(4) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT current_timestamp(),
  `released` tinyint(1) NOT NULL DEFAULT 0,
  `released_date` datetime DEFAULT NULL,
  `released_quantity` int(11) NOT NULL DEFAULT 0,
  `expiration` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `supplies`
--

INSERT INTO `supplies` (`id`, `item`, `classification`, `unit`, `quantity`, `month`, `year`, `active`, `created_at`, `updated_at`, `released`, `released_date`, `released_quantity`, `expiration`) VALUES
(3, 'sample2', 'sam2', 'pack(s)', '400', 'January', '2021', 0, '2024-05-01 03:46:58', '2024-05-01 03:46:58', 1, '2024-06-03 07:13:31', 300, '2024-06-12'),
(4, 'sample31', 'sam21', 'pack(s)', '231', 'July', '2023', 0, '2024-05-01 03:49:01', '2024-05-01 03:49:01', 1, '2024-06-03 07:13:49', 30, '2024-06-29'),
(6, 'sample3', 'sample3', 'pack(s)', '500', 'May', '2024', 1, '2024-05-27 02:22:09', '2024-05-27 02:22:09', 1, '2024-06-03 07:13:58', 500, '2024-06-06');

-- --------------------------------------------------------

--
-- Table structure for table `unit`
--

CREATE TABLE `unit` (
  `id` int(50) UNSIGNED NOT NULL,
  `serial` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `status` varchar(255) NOT NULL,
  `issued_to` varchar(255) NOT NULL,
  `date_issued` date NOT NULL,
  `unit_condition` varchar(255) NOT NULL,
  `reminder` varchar(255) NOT NULL,
  `parts` varchar(255) NOT NULL,
  `month` varchar(12) NOT NULL,
  `year` varchar(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `unit`
--

INSERT INTO `unit` (`id`, `serial`, `description`, `department`, `status`, `issued_to`, `date_issued`, `unit_condition`, `reminder`, `parts`, `month`, `year`) VALUES
(1, '123', 'puncher', 'department', 'broken', 'me', '2024-05-06', 'con', 'tom', 'string', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(50) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `contactnumber` varchar(50) NOT NULL,
  `isAdmin` tinyint(1) NOT NULL DEFAULT 0,
  `Active` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `contactnumber`, `isAdmin`, `Active`) VALUES
(2, 'clea121134', 'clea1234111', 'joy12341556', '09090909097', 1, 0),
(3, 'clea', 'clea123', 'joy123', '09090909090', 0, 1),
(4, 'Darwin', 'admin', '123', '09508090988', 1, 1),
(20, 'user', 'username1', 'pass123', '123456789', 0, 0),
(21, 'username111', 'username11', 'password123', '123456', 0, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `delivery_groups`
--
ALTER TABLE `delivery_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `equipment_groups`
--
ALTER TABLE `equipment_groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registered`
--
ALTER TABLE `registered`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `supplies`
--
ALTER TABLE `supplies`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `unit`
--
ALTER TABLE `unit`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `delivery_groups`
--
ALTER TABLE `delivery_groups`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `equipment_groups`
--
ALTER TABLE `equipment_groups`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `registered`
--
ALTER TABLE `registered`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `supplies`
--
ALTER TABLE `supplies`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `unit`
--
ALTER TABLE `unit`
  MODIFY `id` int(50) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
