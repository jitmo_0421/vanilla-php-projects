<?php
include('user_session.php');
include('warehouse_functions.php');

$isAdmin = checkSession($_SESSION['isAdmin']);
include "conn.php";

if(isset($_POST['generate']))

$report = exportReport($conn, 'supplies',$_POST['month'], $_POST['year'])

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.3/css/dataTables.bootstrap5.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <link rel="stylesheet" href="style/items.css" />
  <link href="lineicons/web-font-files/lineicons.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/bootstrap.bundle.min.js">
  <link rel="stylesheet" href="style/select2.min.css">
  <title>Item Management</title>
  
</head>
<body>
<header class="header-section text-center">
        <h1>Warehouse Management System with 2D Layout</h1>
    </header>
    <div class="wrapper">
         <!-- SIDE NAVBAR -->
         <?php include "side_navbar.php" ?>
        <!-- SIDE NAVBAR -->
       
    <div class="main--content">
        <div class="header--wrapper">
            <div class="header--title">
                <span>Inventory Management</span>
                <h2>Supplies Overview</h2>
                
            </div>
       
        </div>
        <div class="card-container">
            <h3 class="main-title">Generate Supplies Overview</h3>
            <form action="#" id=form method="post">
            <label for="supplygroup">Delivery Month:</label>
            <select name="month" id="month" required>
            <option value="" style="text-align:center;" disabled selected>Select Month</option>
            <?php
            $month = getMonth($conn, 'supplies');
            foreach($month as $month){
              ?>
              <option value="<?= $month['month'] ?>"  style="text-align:center;"><?= $month['month'] ?></option>
              <?php
            }
            ?>
            </select>

            <label for="supplygroup">Delivery Year:</label>
            <select name="year" id="year" required>
            <option value="" style="text-align:center;" disabled selected>Select Year</option>
            
            <?php
            $year = getYear($conn, 'supplies');
            
            foreach($year as $year){
              ?>
              <option value="<?= $year['year'] ?>"  style="text-align:center;"><?= $year['year'] ?></option>
              <?php
            }
            ?>
            </select>
  
  
  
  <input type="submit" id='generate' name='generate' value="Generate">
            </form>
            
        </div>
       
    </div>
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.bootstrap5.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
    <script src="functions/inventory.js"></script>
    <script type="module" src="functions/select2.min.js"></script>
  <script type="module">
    $('#supplygroup').select2();

   
  </script>
</body>
</html>