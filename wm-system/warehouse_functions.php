<?php

function getAllSupplyGroups($conn, $isAdmin){
  if($isAdmin == 1 ){
    $query = "SELECT *  FROM items";
}
else {
$query = "SELECT *  FROM items WHERE active = 1";
}
  $query_run = mysqli_query($conn, $query);

  if (mysqli_num_rows($query_run)>0){
    return $query_run;
  }

  return false;
}

function getSingleSupplyGroup($conn, $id = null){
  if(!$id){
    return false;
  }
  $id = mysqli_real_escape_string($conn, $id);
  $query = "SELECT * FROM items WHERE Name='$id'";
  $query_run = mysqli_query($conn, $query);

  if (mysqli_num_rows($query_run) > 0) {
    return mysqli_fetch_array($query_run);
  }

  return false;
}


function getAllItems($conn, $isAdmin){
  if($isAdmin == 1 ){
    $query = "SELECT * FROM  supplies";
  }
  else{
    $query = "SELECT * FROM  supplies WHERE active = 1";
  }

 $query_run = mysqli_query($conn, $query);

 if (mysqli_num_rows($query_run)>0){
     return $query_run;
 }
 return false;
}

function getItemsSection($conn, $section){
  $query = $conn->query("SELECT * FROM  items WHERE section = '$section' AND active = 1");
  if($query->num_rows > 0){
    return $query;
  }
  return false;
}

function getAllSection($conn){
  $query = $conn->query("SELECT * FROM  items WHERE active = 1 GROUP BY section");
  if($query->num_rows > 0){
    return $query;
  }
  return false;
}

function setItemSection($conn, $id, $section){
  $id = mysqli_real_escape_string($conn, $id);
  $section = mysqli_real_escape_string($conn, $section);
  $query = $conn->query("UPDATE items SET section = '$section' WHERE id = '$id'");
  if($query){
    return true;
  }
  return false;
}

function getSingleItem($conn, $id = null){
  if(!$id){
    return false;
  }
  $id = mysqli_real_escape_string($conn, $id);
            $query = "SELECT * FROM supplies WHERE item='$id'";
            $query_run = mysqli_query($conn, $query);
            if (mysqli_num_rows($query_run) > 0) {
              return mysqli_fetch_array($query_run);
            }
            return false;
}

function getAllDelivery($conn, $isAdmin){
  if($isAdmin == 1 ){
    $query = "SELECT * FROM  delivery_groups";
  } else {
    $query = "SELECT * FROM  delivery_groups WHERE active=1";
  }
  
  $query_run = mysqli_query($conn, $query);
  if (mysqli_num_rows($query_run)>0){
    return $query_run;
  }
  return false;
}

function getAllUsers($conn){
  $query = "SELECT * FROM  users";
  $query_run = mysqli_query($conn, $query);

  if (mysqli_num_rows($query_run)>0){
    return $query_run;
  }
  return false;
}

function getOneUser($conn, $id){
  $query = $conn->query("SELECT * FROM users WHERE id = '$id'");
  if($query->num_rows > 0){
    return $query->fetch_assoc();
  }
  return false;
}

function getAllEquipment($conn, $isAdmin){
  if($isAdmin == 1 ){
    $query = "SELECT * FROM  equipment_groups";
  } else {
    $query = "SELECT * FROM  equipment_groups WHERE active = 1";
  }
  $query_run = mysqli_query($conn, $query);
  if (mysqli_num_rows($query_run)>0){
    return $query_run;
  }
  return false;
}

function updateUser($conn, $id, $state, $field){
  $query = $conn -> query("UPDATE users SET $field = $state WHERE id = '$id'");
  if($query){
    return true;
  } else {
    return false;
  }
}


function exportReport($conn, $table, $month, $year, $type=NULL){
  // null value for month and year
if(!$month){
  $month = date('F');
};
if(!$year) {
  $year = date('Y');
};
// date filename
  $date = strtotime(date('H:i:s'));

  // query function
  $query = $conn->query("SELECT * FROM $table WHERE month = '$month' AND year = '$year' ORDER BY id ASC");
  
// table if statements

if($table === 'delivery_groups' && !$type){
  $fieldArray = array('ID', 'NAME', 'DATE OF DELIVERY', 'REPORT MONTH', 'REPORT YEAR', 'DELIVERED BY', 'RECEIVED BY', 'STATUS', 'CREATED AT');
};

if($table === 'equipment_groups' && !$type){
  $fieldArray = array('ID', 'NAME', 'DESCRIPTION', 'REPORT MONTH', 'REPORT YEAR', 'STATUS', 'CREATED AT');
}

if($table === 'supplies' && !$type){
  $fieldArray = array('ID', 'ITEM', 'CLASSIFICATION', 'UNIT', 'QUANTITY', 'REPORT MONTH', 'REPORT YEAR', 'STATUS', 'CREATED AT');
}

if($table === 'supplies' && $type=='releasing')
{
  $fieldArray = array('ID', 'ITEM', 'CLASSIFICATION', 'UNIT', 'QUANTITY', 'REPORT MONTH', 'REPORT YEAR', 'STATUS', 'RELEASED', 'RELEASED DATE', 'RELEASED QUANTITY', 'EXPIRATION DATE', 'CREATED AT');
}

// end table if statements

  if ($query->num_rows>0){
    $delimiter = ","; 
    if($type){
    $filename = "ex_".$type."-data_". $date ."_". $month ."_". $year .".csv"; 
  } else {
    $filename = "ex_".$table."-data_". $date ."_". $month ."_". $year .".csv";
  }
     
    $f = fopen('php://memory', 'w'); 
    // $fields = array('ID', 'NAME', 'DATE OF DELIVERY', 'REPORT MONTH', 'REPORT YEAR', 'DELIVERED BY', 'RECEIVED BY', 'STATUS', 'CREATED AT'); 
    fputcsv($f, $fieldArray, $delimiter); 
     
    while($row = $query->fetch_assoc()){ 

      // row data
        $status = $row['active'] == 1 ? 'Active' : 'Inactive'; 
        if($table === 'delivery_groups' && !$type){
          $fieldData = array($row['id'], $row['name'], $row['date_of_delivery'], $row['month'], $row['year'], $row['delivered_by'], $row['recieved_by'], $status, $row['created_at']); 
        }
        if($table === 'equipment_groups'  && !$type){
          $fieldData = array($row['id'], $row['name'], $row['description'], $row['month'], $row['year'], $status, $row['Timestamp']);
        }
        if($table === 'supplies'  && !$type){
          $fieldData = array($row['id'], $row['item'], $row['classification'], $row['unit'], $row['quantity'], $row['month'], $row['year'], $status, $row['created_at']);
        }
        if($table === 'supplies' && $type=='releasing'){
          $released = $row['released'] == 1 ? 'Yes' : 'No';
          $released_date = $row['released_date'] == null ? 'N/A' : $row['released_date'];
          $expiration = $row['expiration'] == null ? 'N/A' : $row['expiration'];
          $fieldData = array($row['id'], $row['item'], $row['classification'], $row['unit'], $row['quantity'], $row['month'], $row['year'], $status, $released, $released_date, $row['released_quantity'], $expiration, $row['created_at']);
        }
        // end row data


        $lineData = $fieldData;
        // $lineData = array($row['id'], $row['name'], $row['date_of_delivery'], $row['month'], $row['year'], $row['delivered_by'], $row['recieved_by'], $status, $row['created_at']); 
        fputcsv($f, $lineData, $delimiter); 
    } 

    fseek($f, 0); 
    header('Content-Type: text/csv'); 
    header('Content-Disposition: attachment; filename="' . $filename . '";'); 
    fpassthru($f); 
    
    }
    exit();
  }

function getMonth($conn, $table){

  $query = $conn->query("SELECT month FROM $table GROUP BY month");
  return $query;

}

function getYear($conn, $table){
$query = $conn->query("SELECT year FROM $table GROUP BY year");
return $query;
}

function releaseSupply($conn, $id, $today, $released_quantity, $expiration, $released){
  $quantity_released = $released_quantity + $released;
  $query = $conn->query("UPDATE supplies SET released = 1, released_date = '$today', released_quantity = '$quantity_released', expiration = '$expiration' WHERE id = '$id'");
  if($query){
    return true;
  } else {
    return false;
  }
}
?>