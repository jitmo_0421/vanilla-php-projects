<?php
include('user_session.php');
include('warehouse_functions.php');

$isAdmin = checkSession($_SESSION['isAdmin']);
require "conn.php";
$query = "SELECT *  FROM users";
$result = mysqli_query($conn, $query);

if(isset($_GET['status']) && isset($_GET['state'])){

$status = updateUser($conn, $_GET['status'], $_GET['state'], 'Active');

if($status){
    $_SESSION['message'] = null;
    header("Location: users.php");
    exit();
} else {
    $_SESSION['message'] = "Update Failed";
    header('Location: users.php');
    exit();
}

}
if(isset($_GET['role'])){


    $role = updateUser($conn, $_GET['role'], $_GET['state'], 'isAdmin');

if($role){
    $_SESSION['message'] = null;
    header("Location: users.php");
    exit();
} else {
    $_SESSION['message'] = "Update Failed";
    header('Location: users.php');
    exit();
}

}


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <link rel="stylesheet" href="style/users.css" />
  <link href="lineicons/web-font-files/lineicons.css" rel="stylesheet" />
  <link rel="stylesheet" href="inventories/dashboard.php">
    <link rel="stylesheet" href="inventories/inventory.php">
    <link rel="stylesheet" href="inventories/items.php">
    <link rel="stylesheet" href="inventories/supplygroups.php">
  <link rel="stylesheet" href="js/bootstrap.bundle.min.js">
  <title>User Management</title>
  
</head>
<body>
<header class="header-section text-center">
        <h1>Warehouse Management System with 2D Layout</h1>
    </header>
    <div class="wrapper" id="users">
      <!-- SIDE NAVBAR -->
      <?php include "side_navbar.php" ?>
        <!-- SIDE NAVBAR -->
       
    <div class="main--content">
        <div class="header--wrapper">
            <div class="header--title">
                <span>User Management</span>
                <h2>Dashboard</h2>
            </div>
      
        </div>
        <div class="card-container">
            <h3 class="main-title">Users's Data</h3>
            <div class="card-wrapper">
                <div class="payment-card light-red">
                <div class="card-header">
                        
                    <div class="amount">
                        <span class="title">Total Number Active Users</span>
                        <span class="amount-value">
                        <?php
                        $sql = "SELECT SUM(Active) AS total_active FROM users";
                        $result = $conn->query($sql);
                        
                        if ($result->num_rows > 0) {
                            // Output data of each row
                            $row = $result->fetch_assoc();
                            $totalactive = $row["total_active"];
                            echo $totalactive;
                        } else {
                            echo "0 results";
                        }
                           
                         ?>
                        </span>
                    </div>
                    <i class="lni lni-users icon dark-red"></i> 
                </div>
                        </div>
                <div class="payment-card light-purple">
                    <div class="card-header">
                    <div class="amount">
                        <span class="title">Total Number Inactive Users</span>
                        <span class="amount-value">0</span>
                    </div>
                    <i class="lni lni-users icon dark-purple"></i>
                </div>
                    </div>  
            </div>
        </div>

        <div class="tabular-wrapper">
            <h3 class="main-title">
                Users
            </h3>
            <div class="table-container">
                <table>
                    <thead>
                        <tr>
                       <th>Set Status</th>
                       <th>Set Role</th>
                          <th>Name</th>
                          <th>Userame</th>
                          <th>Password</th>
                          <th>Contactnumber</th>
                          <th>Status</th>
                         <th>Role</th>
                        
                        </tr>
                        <tbody>
                           
                           
                           <?php
                            $users = getAllUsers($conn);

                            if ($users) {
                                foreach($users as $users){

                                    ?>
                                    <tr>
                                    <td><a href="users.php?status=<?= $users['id'] ?>&state=<?= $users['Active'] == 1 ? 0 : 1 ?>" class="btn <?= $users['Active'] == 1 ? 'btn-danger' : 'btn-success' ?>" <?= $_SESSION['userId'] == $users['id'] ? 'hidden' : ''; ?>><?= $users['Active'] == 1 ? 'Deactivate' : 'Activate'?></a></td>
                                    <td><a href="users.php?role=<?= $users['id'] ?>&state=<?= $users['isAdmin'] == 1 ? 0 : 1 ?>" class="btn <?= $users['isAdmin'] == 1 ? 'btn-danger' : 'btn-success' ?>" onclick="return confirm('Are you sure you want to change role?')"  <?= $_SESSION['userId'] == $users['id'] ? 'hidden' : ''; ?>><?= $users['isAdmin'] == 1 ? 'To User' : 'To Admin'?></a></td>
                                        <td><?= $users['name']?></td>
                                        <td><?= $users['username']?></td>
                                        <td><?= $users['password']?></td>
                                        <td><?= $users['contactnumber']?></td>
                                        <td><?= $users['Active'] == 1 ? 'Active' : 'Inactive'?></td>
                                        <td><?= $users['isAdmin'] == 1 ? 'Admin' : 'User' ?></td>
                                    </tr>
                                    <?php
                                }
                            }else{
                                echo "<h5> No record Found</h5>";
                            }
                            ?>
                               
                             
                           
                        </tbody>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
    <script src="functions/inventory.js"></script>
</body>
</html>