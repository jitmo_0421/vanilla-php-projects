<?php
include('user_session.php');
include('warehouse_functions.php');

$isAdmin = checkSession($_SESSION['isAdmin']);
include "conn.php";
$query = "SELECT *  FROM items";
$result = mysqli_query($conn, $query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.3/css/dataTables.bootstrap5.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <link rel="stylesheet" href="style/inventory.css" />
  <link href="lineicons/web-font-files/lineicons.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/bootstrap.bundle.min.js">
  <title>Inventory Management</title>
  
</head>
<body>
<header class="header-section text-center">
        <h1>Warehouse Management System with 2D Layout</h1>
    </header>
    <div class="wrapper">
         <!-- SIDE NAVBAR -->
         <?php include "side_navbar.php" ?>
        <!-- SIDE NAVBAR -->
    <div class="main--content">
        <div class="header--wrapper">
            <div class="header--title">
                <span>Inventory Management</span>
                <h2>Dashboard</h2>
            </div>
      
        </div>
        <div class="card-container">
            <h3 class="main-title">Inventory's Data</h3>
            <div class="card-wrapper">
                <div class="payment-card light-red">
                <div class="card-header">
                        
                    <div class="amount">
                        <span class="title">Total Number of Products</span>
                        <span class="amount-value">
                        <?php
                        $sql = "SELECT SUM(quantity) AS total_quantity FROM supplies";
                        $result = $conn->query($sql);
                        
                        if ($result->num_rows > 0) {
                            // Output data of each row
                            $row = $result->fetch_assoc();
                            $totalQuantity = $row["total_quantity"];
                            echo $totalQuantity;
                        } else {
                            echo "0 results";
                        }
                           
                         ?>

                        </span>
                    </div>
                    <i class="lni lni-producthunt icon dark-red"></i> 
                </div>
                        </div>
                <div class="payment-card light-purple">
                    <div class="card-header">
                    <div class="amount">
                        <span class="title">Total Number of Equipments</span>
                        <span class="amount-value">
                        <?php
                        $sql = "SELECT SUM(id) AS total_quantity FROM equipment_groups";
                        $result = $conn->query($sql);
                        
                        if ($result->num_rows > 0) {
                            // Output data of each row
                            $row = $result->fetch_assoc();
                            $totalQuantity = $row["total_quantity"];
                            echo $totalQuantity;
                        } else {
                            echo "0 results";
                        }
                           
                         ?>

                        </span>
                    </div>
                    <i class="lni lni-producthunt icon dark-purple"></i>
                </div>
                    </div>
                    
                <div class="payment-card light-green">
                    <div class="card-header">
                    <div class="amount">
                        <span class="title">Total Products Released</span>
                        <span class="amount-value">
                        <?php
                        $sql = "SELECT SUM(quantity) AS total_quantity FROM supplies";
                        $result = $conn->query($sql);
                        
                        if ($result->num_rows > 0) {
                            // Output data of each row
                            $row = $result->fetch_assoc();
                            $totalQuantity = $row["total_quantity"];
                            echo $totalQuantity;
                        } else {
                            echo "0 results";
                        }
                           
                         ?>
                        </span>
                    </div>
                    <i class="lni lni-producthunt icon dark-green"></i>
                </div>
                    </div>
                  
            </div>
    
        </div>
        <div class="tabular-wrapper">
            <h3 class="main-title">
                Products Released
            </h3>
            <div class="table-container">
            <table id="example" class="table table-striped" style="width:100%">
            <thead>
                <tr>
                    <th>Item / Type</th>
                    <th>Quantity</th>
                    <th>Issued To</th>
                    <th>Issued By</th>
                    <th>Time Stamp</th>
                  
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>sample</td>
                    <td>sample</td>
                    <td>sample</td>
                    <td>61</td>
                    <td>2011-04-25</td>
                  
                </tr>
               
            </tbody>
            <tfoot>
                <tr>
                <th>Item / Type</th>
                    <th>Quantity</th>
                    <th>Issued To</th>
                    <th>Issued By</th>
                    <th>Time Stamp</th>
                </tr>
            </tfoot>
        </table>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.bootstrap5.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
    <script src="functions/inventory.js"></script>
    <script></script>
</body>
</html>