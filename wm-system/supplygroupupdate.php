<?php
include('user_session.php');
include('warehouse_functions.php');

$isAdmin = checkSession($_SESSION['isAdmin']);

if($isAdmin == 0){
    header('location:dashboard.php');
}

require "conn.php";

/*$query = "SELECT *  FROM items";
$result = mysqli_query($conn, $query);*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <link rel="stylesheet" href="style/supplygroups.css" />
  <link href="https://cdn.lineicons.com/4.0/lineicons.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/bootstrap.bundle.min.js">
  <title>SupplyGroups Management</title>
  
</head>
<body>
<header class="header-section text-center">
        <h1>Warehouse Management System with 2D Layout</h1>
    </header>
    <div class="wrapper">
          <!-- SIDE NAVBAR -->
          <?php include "side_navbar.php" ?>
        <!-- SIDE NAVBAR -->
       
    <div class="main--content">
        <div class="header--wrapper">
            <div class="header--title">
                <span>Inventory Management</span>
                <h2>Supply Groups</h2>
            </div>
        <div class="user--info">
            <div class="search-box">
            <i class="lni lni-search-alt"></i>
        <input type="text" placeholder="Search">
        </div>
        <img src="imgs/user-icon.jpg" alt="">
            </div>  
        </div>

        <div class="card-container">
            <h3 class="main-title">Update Supply Groups</h3> 
           <?php include('message.php')?>

           <?php
               $item = getSingleSupplyGroup($conn, $_GET['id']);
               if($item){
              ?>
             
            <form action="supplygroupfunc.php" id="form" method="post">
                <input type="hidden" name="id" value="<?= $item['id'];?>">
            <label for="Name">Name:</label>
  <input type="text" id="Name" value="<?= $item['Name'];?>" name="Name">
  <label for="Description">Description:</label>
  <input type="text" id="Description" value="<?= $item['Description'];?>" name="Description">
  <label for="status">Status:</label>
  <select name="status" id="status" value="<?= $item['active']; ?>" class="form-select">
                <option value="1" <?= $item['active'] == 1 ? 'disabled selected' : '' ?>>Active</option>
                <option value="0" <?= $item['active'] == 0 ? 'disabled selected' : '' ?>>Inactive</option>
</select>
 
  <input type="submit" name="Update" value="Update">
            </form>
             
            <?php
            }
            else{
                echo "<h4>No Such ID Found.</h4>";
            }
           ?>
        </div>
  
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
    <script src="functions/update.js"></script>
</body>
</html>