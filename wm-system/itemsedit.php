<?php
include('user_session.php');
include('warehouse_functions.php');

$isAdmin = checkSession($_SESSION['isAdmin']);
if($isAdmin == 0){
    header('location:dashboard.php');
}
require "conn.php";
/*$query = "SELECT *  FROM items";
$result = mysqli_query($conn, $query);*/
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <link rel="stylesheet" href="style/supplygroups.css" />
  <link href="lineicons/web-font-files/lineicons.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/bootstrap.bundle.min.js">
  <title>SupplyGroups Management</title>
  
</head>
<body>
<header class="header-section text-center">
        <h1>Warehouse Management System with 2D Layout</h1>
    </header>
    <div class="wrapper">
         <!-- SIDE NAVBAR -->
         <?php include "side_navbar.php" ?>
        <!-- SIDE NAVBAR -->
       
    <div class="main--content">
        <div class="header--wrapper">
            <div class="header--title">
                <span>Inventory Management</span>
                <h2>Supply Items</h2>
            </div>
        <div class="user--info">
            <div class="search-box">
            <i class="lni lni-search-alt"></i>
        <input type="text" placeholder="Search">
        </div>
        <img src="imgs/user-icon.jpg" alt="">
            </div>  
        </div>

        <div class="card-container">
            <h3 class="main-title">Update Supply Items</h3> 
           <?php include('message.php')?>

           <?php
           $item = getSingleItem($conn, $_GET['id']);
        //    if (isset($_GET['id'])) {
        //     $id = mysqli_real_escape_string($conn, $_GET['id']);
        //     $query = "SELECT * FROM supplies WHERE item='$id'";
        //     $query_run = mysqli_query($conn, $query);

        //     if (mysqli_num_rows($query_run) > 0) {
        //         $item = mysqli_fetch_array($query_run);
               if($item){
              ?>
             
            <form action="itemsfunc.php" id="form" method="post">
                <input type="hidden" name="id" value="<?= $item['id'];?>">
            
            <label for="itemtype">Item / Type:</label>
  <input type="text" id="itemtype" value="<?= $item['item'];?>" name="itemtype">
  <label for="class">Classification:</label>
  <input type="text" id="class" value="<?= $item['classification'];?>" name="class">
  
  <label for="unit">Unit:</label>
  <input type="text" id="unit" value="<?= $item['unit'];?>" name="unit" >
  <label for="quan">Quantity:</label>
  <input type="text" id="quan" name="quan" value="<?= $item['quantity'];?>" step="0.01">
  <label for="status">Status:</label>
  <select name="status" id="status" value="<?= $item['active']; ?>" class="form-select">
                <option value="1" <?= $item['active'] == 1 ? 'disabled selected' : '' ?>>Active</option>
                <option value="0" <?= $item['active'] == 0 ? 'disabled selected' : '' ?>>Inactive</option>
</select>
  <input type="submit" name="Update" value="Update">
            </form>
             
            <?php
            }else{
                echo "<h4>No Such ID Found.</h4>";
            }
           ?>
        </div>
  
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
    <script src="functions/update.js"></script>
</body>
</html>