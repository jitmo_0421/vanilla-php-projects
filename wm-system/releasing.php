<?php
include('user_session.php');
include('warehouse_functions.php');
require "conn.php";


$isAdmin = checkSession($_SESSION['isAdmin']);

$today = date('Y-m-d H:i:s');

if(isset($_POST['release'])){
    $releasing = releaseSupply($conn, $_POST['item_id'], $today, $_POST['released_quantity'], $_POST['expiration'], $_POST['released']);


    if(!$releasing){
        $_SESSION['message'] = "Releasing Failed";
        header("Location: releasing.php");
        exit(0);
    }
    $_SESSION['message'] = NULL;
    header("Location: releasing.php");
    exit(0);
}

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.3/css/dataTables.bootstrap5.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <link rel="stylesheet" href="style/items.css" />
  <link href="lineicons/web-font-files/lineicons.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/bootstrap.bundle.min.js">
  <title>Item Management</title>
  
</head>
<body>
<header class="header-section text-center">
        <h1>Warehouse Management System with 2D Layout</h1>
    </header>
    <div class="wrapper">
         <!-- SIDE NAVBAR -->
         <?php include "side_navbar.php" ?>
        <!-- SIDE NAVBAR -->
       
    <div class="main--content">
        <div class="header--wrapper">
            <div class="header--title">
                <span>Inventory Management</span>
                <h2>Supply Items</h2>
                
            </div>
      
        </div>
        <div class="card-container">
            <h1 class="main-title">Supply Releasing</h1>
            <p class="text-danger"><?= isset($_SESSION['message']) ? $_SESSION['message'] : '' ?></p>
            
        
            
        </div>
        <div class="tabular-wrapper">
            <h3 class="main-title">
                Items
            </h3>
            <div class="table-container">
            <table id="example" class="table table-striped" style="width:100%">
            <thead>
                <tr>
                   <?= $isAdmin == 1 ?  '<th>Action</th>' : ''  ?> 
                    <th>Item / Type</th>
                    <th>Classification</th>
                    <th>Unit</th>
                    <th>Quantity</th>
                    <th>Released</th>
                    <th>Released Date</th>
                    <th>Released Quantity</th>
                    <th>Expiration Date</th>
                    <th>Timestamp</th>
                  
                </tr>
            </thead>
            <tbody>
            <?php
                            // $query = "SELECT * FROM  supplies";
                            // $query_run = mysqli_query($conn, $query);

                            // if (mysqli_num_rows($query_run)>0) {
                                $items = getAllItems($conn, $isAdmin);
                                if($items){
                                foreach($items as $items){

                                    ?>
                                    <tr>
                                    <?php if($isAdmin == 1) { ?><td>
                                        
                                        <button name="Update" style="color:blue; background:none;" <?= $items['released'] == 1 && $items['released_quantity'] == $items['quantity'] ? 'disabled' : '' ?>  data-bs-toggle="modal" data-bs-target="#<?= $items['id']?>_modal"><?= $items['released'] == 1 && $items['released_quantity'] == $items['quantity'] ? 'Released' : 'Release' ?></button>
                                       
                                       
                                      
                                    </td> <?php } ?>
                                       
                                        <td><?= $items['item']?></td>
                                        <td><?= $items['classification']?></td>
                                        <td><?= $items['unit']?></td>
                                        <td><?= $items['quantity']?></td>
                                        <td><?= $items['released'] == 1 ? 'Yes' : 'No'?></td>
                                        <td><?= $items['released_date'] == NULL ? 'N/A' : $items['released_date']?></td>
                                        <td><?= $items['released_quantity'] == 0 ? 'N/A' : $items['released_quantity'].''.$items['unit'] ?></td>
                                        <td><?= $items['expiration'] == NULL ? 'N/A' : $items['expiration']?></td>
                                        <td><?= $items['created_at']?></td>
                                        
                                      
                                     
                                    </tr>

<!-- Modal -->
<div class="modal fade" id="<?= $items['id']?>_modal" tabindex="-1" aria-labelledby="<?= $items['id']?>_modalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"><?= $items['item']?></h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <form class="form" action="#" method="POST">
      <div class="modal-body">
        <p><strong>Item Name:</strong> <?= $items['item'] ?></p>
        <p><strong>Item Classification:</strong> <?= $items['classification'] ?></p>
        <input type="hidden" name="item_id" id="item_id" value="<?= $items['id'] ?>">
        <input type="hidden" name="released" id="released" value="<?= $items['released_quantity'] ?>">
        <div class="form-group my-2">
        <label for="released_quantity" class="form-label">Released Quantity</label>
            <div class="d-flex flex-row flex-wrap">
                <input class="form-range" type="range" name="released_quantity" id="released_quantity" min='0' max='<?= abs($items['quantity'] - $items['released_quantity']) ?>' required oninput="this.nextElementSibling.value = this.value">
                <output>0</output> <?= $items['unit'] ?>
                
            </div>
        </div>
        <div class="d-flex flex-wrap flex-column my-2">
            <label for="expiration" class="form-label">Expiration Date</label>
            <input type="date" name="expiration" id="expiration" value="<?= $items['expiration'] ?>" required>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
        <button type="submit" name="release" id="release" class="btn btn-primary">Release</button>
        </form>
      </div>
    </div>
  </div>
</div>
                                    <?php
                                }
                            }else{
                                echo "<h5> No record Found</h5>";
                            }
                            ?>
            </tbody>
            <tfoot>
                <tr>
                <?= $isAdmin == 1 ?  '<th>Action</th>' : ''  ?>   
                    <th>Item / Type</th>
                    <th>Classification</th>
                    <th>Unit</th>
                    <th>Quantity</th>
                    <th>Released</th>
                    <th>Released Date</th>
                    <th>Released Quantity</th>
                    <th>Expiration Date</th>
                    <th>Timestamp</th>
                </tr>
            </tfoot>
        </table>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.bootstrap5.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
    <script src="functions/inventory.js"></script>
</body>
</html>