<?php
include('user_session.php');

$isAdmin = checkSession($_SESSION['isAdmin']);
require "conn.php";
$query = "SELECT *  FROM items";
$result = mysqli_query($conn, $query);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/2.0.3/css/dataTables.bootstrap5.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-QWTKZyjpPEjISv5WaRU9OFeRpok6YctnYmDr5pNlyT2bRjXh0JMhjY6hW+ALEwIH" crossorigin="anonymous">
  <link rel="stylesheet" href="style/supplygroups.css" />
  <link href="lineicons/web-font-files/lineicons.css" rel="stylesheet" />
  <link rel="stylesheet" href="js/bootstrap.bundle.min.js">
  <title>SupplyGroups Management</title>
  
</head>
<body>
<header class="header-section text-center">
        <h1>Warehouse Management System with 2D Layout</h1>
    </header>
    <div class="wrapper">
         <!-- SIDE NAVBAR -->
         <?php include "side_navbar.php" ?>
        <!-- SIDE NAVBAR -->
       
    <div class="main--content">
        <div class="header--wrapper">
            <div class="header--title">
                <span>Inventory Management</span>
                <h2>New Supply Groups</h2>
            </div>
      
        </div>

      
  
        <div class="tabular-wrapper">
            <h3 class="main-title">
                Products
            </h3>
            <div class="table-container">
            <table id="example" class="table table-striped" style="width:100%">
            <thead>
                <tr>
                    <th>Action</th>    
                    <th>Serial</th>
                    <th>Unit Description</th>
                    <th>Department</th>
                    <th>Status</th>
                    <th>Issued To</th>
                    <th>Date Issued</th>
                    <th>Condition</th>
                    <th>Reminder</th>
                    <th>Parts</th>
                  
                </tr>
            </thead>
            <tbody>
           <?php
                          /*  $query = "SELECT * FROM  items";
                            $query_run = mysqli_query($conn, $query);

                            if (mysqli_num_rows($query_run)>0) {
                                foreach($query_run as $items){

                                    ?>
                                    <tr>
                                    <td>
                                        <form action="supplygroupfunc.php" method="POST" class="d-inline">
                                      <button type="submit" name="delete_group" value="<?=$items['id']?>;" style="color:red; background:none; border:none;">Delete</button>
                                        </form>
                                        <a href="supplygroupupdate.php?id=<?= $items['Name'];?>" type="submit" name="Update" style="color:blue; background:none;" >Edit</a>
                                        <a href="supplygroupupdate.php?id=<?= $items['Name'];?>" type="submit" name="Update" style="color:rgb(15, 99, 15); background:none;" >Deliveries</a>
                                       
                                      
                                    </td>
                                        <td><?= $items['Name']?></td>
                                        <td><?= $items['Description']?></td>
                                        <td><?= $items['Timestamp']?></td>
                                        <td>xoxo</td>
                                        <td>xoxo</td>
                                    </tr>
                                    <?php
                                }
                            }else{
                                echo "<h5> No record Found</h5>";
                            }*/
                            ?>
                            <tr>
                                <td>
                                <form action="supplygroupfunc.php" method="POST" class="d-inline">
                                      <button type="submit" name="delete_group" value="<?=$items['id']?>;" style="color:red; background:none; border:none;">Delete</button>
                                        </form>
                                        <a href="supplygroupupdate.php?id=<?= $items['Name'];?>" type="submit" name="Update" style="color:blue; background:none;" >Edit</a>
                                        
                                </td>
                                <td>Serial</td>
                    <td>Unit Description</td>
                    <td>Department</td>
                    <td>Status</td>
                    <td>Issued To</td>
                    <td>Date Issued</td>
                    <td>Condition</td>
                    <td>Reminder</td>
                    <td>Parts</td>
                            </tr>
            </tbody>
            <tfoot>
            <tr>
                    <th>Action</th>    
                    <th>Serial</th>
                    <th>Unit Description</th>
                    <th>Department</th>
                    <th>Status</th>
                    <th>Issued To</th>
                    <th>Date Issued</th>
                    <th>Condition</th>
                    <th>Reminder</th>
                    <th>Parts</th>
                  
                </tr>
            </tfoot>
        </table>
               
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.7.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/5.3.0/js/bootstrap.bundle.min.js"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.js"></script>
    <script src="https://cdn.datatables.net/2.0.3/js/dataTables.bootstrap5.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe"
        crossorigin="anonymous"></script>
    <script src="functions/update.js"></script>
</body>
</html>